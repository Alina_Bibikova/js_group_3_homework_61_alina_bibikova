import React, { Component } from 'react';
import GeneralContent from "./containers/GeneralContent/GeneralContent";

class App extends Component {
  render() {
    return <GeneralContent />
  }
}

export default App;
