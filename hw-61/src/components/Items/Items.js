import React from 'react';
import './Items.css';

const Items = props => {
    return (
        <div>
            <div className='item' onClick={props.clicked}>
                <span>{props.countryName}</span>
            </div>
        </div>
    );
};

export default Items;