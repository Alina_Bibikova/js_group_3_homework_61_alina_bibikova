import React from 'react';
import './InfoCountry.css';

const InfoCountry = props => {
    return (
            <div className='infoCountry'>
                <p><strong>Name: </strong> {props.counytyName}</p>
                <p><strong>Capital: </strong> {props.capital}</p>
                <p><strong>Population: </strong> {props.population}</p>
                <p><strong>Borders: </strong> {props.borders.join(" ")}</p>
                <p><strong>Region: </strong>{props.region}</p>
                <p><strong>Subregion: </strong>{props.subregion}</p>
                <img alt={props.counytyName} src={props.flag} width="350px" height="150px"/>
            </div>
        );
};

export default InfoCountry;





