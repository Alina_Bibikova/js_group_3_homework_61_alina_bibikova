import React, { Component, Fragment } from 'react';
import axios from 'axios';
import './GeneralContent.css';
import InfoCountry from "../../components/InfoCountry/InfoCountry";
import Items from "../../components/Items/Items";

class GeneralContent extends Component {
    state = {
        countries: [],
        countryInfo: {}
    };

    BASE_URL = 'https://restcountries.eu/rest/v2/all?fields=name';
    COUNTRY_URL = 'https://restcountries.eu/rest/v2/name/';
    ALPHA_URL = 'https://restcountries.eu/rest/v2/alpha/';

    componentDidMount () {
        axios.get(this.BASE_URL)
        .then(countries => {
            this.setState({countries: countries.data})
        }).catch(error => {
            console.log(error);
        })
    }

    postSelectedHandler = name => {
        axios.get(this.COUNTRY_URL + name)
            .then(countries => {
                Promise.all(countries.data[0].borders.map(border => {
                    return axios.get(this.ALPHA_URL + border)
                })).then(response => {
                    let countryInfo = countries.data[0];
                    countryInfo.borders = response.map(country => country.data.name);
                    this.setState({countryInfo})
                })
            }).catch(error => {
            console.log(error);
        })

    };

    render() {
        return (
            <Fragment>
                <div className='container'>
                    <div className="items">
                        {this.state.countries.map((post, key) =>
                            <Items
                                key={key}
                                countryName={post.name}
                                clicked={() => this.postSelectedHandler(post.name)}
                            >
                            </Items>
                        )
                        }
                    </div>
                    {Object.keys(this.state.countryInfo).length > 0 ?
                    <InfoCountry
                        counytyName={this.state.countryInfo.name}
                        capital={this.state.countryInfo.capital}
                        population={this.state.countryInfo.population}
                        borders={this.state.countryInfo.borders}
                        region={this.state.countryInfo.region}
                        subregion={this.state.countryInfo.subregion}
                        flag={this.state.countryInfo.flag}
                        /> : <h2>Here is the information about countries!</h2>
                    }
                    </div>
            </Fragment>
        )
    }
}

export default GeneralContent;
